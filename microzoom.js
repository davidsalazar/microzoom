function MicroZoom(element, options)
{
	"use strict";

	if (!element)
		return;

	var zoomedImg, last;
	var client = {};
	var options = options || {};
	var sizes = ['fit', 'width', 'original'];

	var size = options.size && sizes.indexOf(options.size) != -1 ? options.size : 'original';

	var elements = document.querySelectorAll(element);
	var wrapper = document.createElement('div');
	wrapper.className = 'microzoom-wrapper';
	wrapper.style.display = 'none';

	var img = document.createElement('img');
	img.className = 'microzoom-img';
	wrapper.appendChild(img);

	var a = document.createElement('a');
	a.className = 'microzoom-mode';
	
	var i = document.createElement('i');
	a.appendChild(i);
	wrapper.appendChild(a);

	document.body.appendChild(wrapper);

	function debug(msg)
	{
		console.log(msg);
	}

	function scrollPosition(set)
	{
		if (set) {
			document.documentElement.scrollLeft = document.body.scrollLeft = set.scrollLeft;
			document.documentElement.scrollTop = document.body.scrollTop = set.scrollTop;
		}
		else {
			var scrollLeft = window.pageXOffset || document.documentElement.scrollLeft, scrollTop = window.pageYOffset || document.documentElement.scrollTop;
			return {'scrollLeft': scrollLeft, 'scrollTop': scrollTop};
		}
		
	}

	// returning best fit dimensions and position
	function imgFitSize(img, onlyDims)
	{
		var smallerImg = imgSmaller(img), position = scrollPosition(), w = smallerImg ? img.naturalWidth : window.innerWidth, h = smallerImg ? img.naturalHeight : window.innerHeight, viewPortRatio = window.innerWidth / window.innerHeight, ratio = img.naturalWidth / img.naturalHeight, left = 0, top = position.scrollTop;

		if (viewPortRatio > ratio) {
			w = h * ratio;
			left = (window.innerWidth - w) / 2;
			if (smallerImg)
				top = top + ((window.innerHeight - h) / 2);
		}
		else {
			h = w / ratio;
			top = top + ((window.innerHeight - h) / 2);
			if (smallerImg)
				left = (window.innerWidth - w) / 2;
		}

		var dims = {'width': w, 'height': h, 'left': left, 'top': top};

		debug(dims);

		if (!onlyDims) {
			for (var k in dims)
				if (dims.hasOwnProperty(k))
					img.style[k] = dims[k] + 'px';
		}

		return dims;
	}

	function imgSmaller(img)
	{
		return img.naturalWidth < window.innerWidth && img.naturalHeight < window.innerHeight; 
	}

	function zoom(e) {
		e.preventDefault();

		if (!this.isZoomed) {

			this.isZoomed = true;
			zoomedImg = last = this;

			// save scrollPosition
			client.scrollPosition = scrollPosition();

			img.style.display = 'none';
			if (['fit', 'width'].indexOf(size) != -1) {
				debug('fit start');
				document.getElementsByTagName('body')[0].style.overflow = 'hidden';
				img.classList.add('microzoom-img-fit');
				debug('img onload start');
				img.onload = function() {
					debug('img onload success');
					if (!zoomedImg)
						return true;

					debug('img fitsize calc');
					var dims = imgFitSize(img);

					a.style.top = dims['top'] + 'px';
					if (dims['width'] < window.innerWidth)
						a.style.left = (dims['left'] + dims['width']) - 42 + 'px';
					else
						a.style.left = dims['width'] - 42 + 'px';	

					i.className = 'icon-resize-full';
					img.style.display = 'block';
				};
			}
			else if (size == 'original') {
				img.onload = function() {
					debug(imgSmaller(img));
					if (imgSmaller(img))  {
						imgFitSize(img);
						document.getElementsByTagName('body')[0].style.overflow = 'hidden';
					}
					else {
						scrollPosition({'scrollLeft': 0, 'scrollTop': 0});	
					}

					var dims = imgFitSize(img, true);
					a.style.top = 0;
					a.style.left = img.naturalWidth - 42 + 'px';
					i.className = 'icon-resize-small';

					img.style.display = 'block';
				};
			}
						
			wrapper.style.display = 'block';
			console.log(wrapper.style.display);
			img.src = this.href;

			setTimeout(function() {
				wrapper.classList.add('microzoom-wrapper-show');
			}, 0);

		} else {

			this.isZoomed = false;
			zoomedImg = null;

			img.onload = null;
			img.style.cssText = '';
			img.src = '';

			// restore scrollPosition
			scrollPosition(client.scrollPosition);

			document.getElementsByTagName('body')[0].style.overflow = 'visible';

			wrapper.classList.remove('microzoom-wrapper-show');
			setTimeout(function() {
				wrapper.style.display = 'none';
			}, 0);
		}
	}

	Array.prototype.forEach.call(elements, function(el, i) {
		el.addEventListener('click', zoom);
	});

	//zoomOut on scroll
	function zoomOut() {
		if (zoomedImg)
			zoomedImg.click();
	}
	
	wrapper.addEventListener('click', zoomOut);
	img.addEventListener('click', zoomOut);
	//window.addEventListener('scroll', zoomOut);
	window.addEventListener('resize', zoomOut);

	document.addEventListener('keyup', function(e) {
		if (!zoomedImg)
			return true;

		var key = e.key;

		if (key == 'o') {
			size = 'original';
			zoomOut();
			setTimeout(function() {
				last.click();
			}, 0);
			
		}
		else if (key == 'f') {
			size = 'fit';
			zoomOut();
			setTimeout(function() {
				last.click();
			}, 0);
		}

		else if (key === 'Escape' || key === 'Esc')
			zoomOut();
	});	
	

}